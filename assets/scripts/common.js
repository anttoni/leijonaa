/**
 * Common JS controller.
 */

// Use jQuery as $ within Common file scope.
const $ = jQuery;

/**
 * Class Common
 */
class Common {

    /**
     * Class constructor is for binding class properties.
     */
    constructor() {
    }

    cache() {
        this.archive_list = document.getElementById('js-archive');
        this.archive_loading = document.getElementById('js-archive-loading');

        this.archive_list_error = document.getElementById('js-archive-error');

        this.menu_button = document.getElementById('js-toggle-menu');
        this.menu_hamburger = document.getElementById('js-menu-hamburger');

    }

    sidebar() {
        if ( this.archive_list ) {
            this.sidebar_archive();
        }
    }


    instagram() {
        dp("Footer/instagram", {
            tidy: true,
            partial: 'instagram',
            success: function( data ) {
                const feed = document.getElementById('instagram__feed');
                feed.innerHTML = data;
            },
            error: function( error ) {
                console.log(error);
            }
        });
    }

    /**
     * Generate sidebar archive from JSON data.
     */
    sidebar_archive() {
        dp('MiddleModel/get_archive', {
            tidy: true,
            partial: 'archive-list',
            success: data => {
                this.archive_list.innerHTML = data;
                this.archive_loading.remove();
            },
            error: data => {
                this.archive_list_error.innerTHML = ljn_js_translations.archive_error_message;
                this.archive_loading.remove();
            },
        });
    }

    toggle_menu() {
        this.menu_button.classList.toggle('open');
        this.menu_hamburger.classList.toggle('open');
    }


    toggle_archive_list( e ) {
        if( e.target.parentNode.classList.contains('active') ) {
            e.target.parentNode.classList.add('close');
            window.setTimeout(this.closing_animation, 500, e.target.parentNode, 'close');
        }
        e.target.parentNode.classList.toggle('active');
    }


    closing_animation( target, className ) {
        target.classList.remove(className);
    }

    /**
     * Offers better e.preventDefault support.
     * @param e Event.
     */
    static stop(e) {
        e.preventDefault ? e.preventDefault() : ( e.returnValue = false );
    }

    static getId(e) {
        if( e.target === 'a') {
            id = e.target.dataset.id;
            return id;
        }

        let target = $( e.target );
        let link = target.closest('a')[0];
        let id = link.dataset.id;
        return id;
    }

    addLike(e) {
        console.log(e);
        let id = Common.getId(e);
        this.editLike('add', id );
    }

    removeLike(e) {
        let id = e.target.dataset.id;
        this.editLike('remove', id);
    }

    editLike( action, id ) {
        console.log( action + '_likes');
        $.ajax({
            url: localData.ajaxurl, // or example_ajax_obj.ajaxurl if using on frontend
            type: 'post',
            data: {
                'action': action + '_likes',
                'id': id,
            },
            success:function(data) {
                // This outputs the result of the ajax request
                console.log(data);
            },
            error: function(error){
                console.log(error);
            }
        });
    }

    /**
     * Run when the document is ready.
     */
    docReady() {
        this.cache();
        this.sidebar();
        this.instagram();
    }
}

module.exports = Common;
