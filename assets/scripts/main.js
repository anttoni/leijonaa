
/**
 * Theme JS building
 */

// Require 3rd party libraries

// Only require babel-polyfill if hasn't been loaded before.
if ( ! window._babelPolyfill ) {
    require( 'babel-polyfill' );
}

require( __dirname + '/modernizr.js' );

// This is used to watch for changes in *.hbs files in the styles directory.
// This way the styleguide automatically gets regenerated when handlebars template file is changed.
//function requireAll( r ) {
//    r.keys().forEach( r );
//}
// requireAll( require.context( '../styles/', true, /\.hbs$/ ) );

// Export the theme controller for global usage.
window.Theme = require( __dirname + '/theme.js' );

// Require main style file here for concatenation.
require( __dirname + '/../styles/main.scss' );
