<?php
/**
 * Initialize the theme settings loading.
 */

 if ( ! class_exists('DustPress') ) {
    // Include Dustpress.
    require_once 'packages/dustpress/dustpress.php';
}
// Include twitteroauth autoloader
require_once 'packages/twitteroauth/autoload.php';

// Use dustpress
dustpress();

// Require all function files under /lib.
$lib_path = dirname( __FILE__ ) . '/lib/';
// List your /lib files here.
$includes = [
    'utilities.php',          // Utility functions.
	'acf-theme-settings.php', // Theme settings
	'acf-article-fields.php', // Post fields
    'acf-menu-settings.php',  // Menu settings
    'archive.php',            // Sidebar archive cache control.
    'extras.php',             // Custom functions
    'excerpt.php',            // Excerpt
    'images.php',             // Image functions,
    'setup.php',              // Theme setup
    'svg.php',                // Svg helper.
    'twitter.php',            // Twitter API
    'js-translations.php',    // Localized strings for JavaScript
    'sidebar.php',            // Sidebar functions.
    'comments.php',           // Comment settings
    'like.php',               // Like handling
    'instagram.php',          // Instagram
    'hooks.php',              // Actions and filters
    'ljn-pagination.php',     // Pagination helper
];

foreach ( $includes as $file ) {
    $file_path = $lib_path . $file;
    if ( is_file( $file_path ) ) {
        require $file_path;
    }
}
