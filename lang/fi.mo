��    9      �  O   �      �  �   �  )   �     �  !   �     �          
          +  
   7  _   B  
   �     �     �  O   �     !     %     +  	   I     S     i  K   n     �  �   �     W     d     i     r     z     �  F   �     �     �     �     �     �  	   	     	     '	     =	     C	     J	  �   O	     �	  ?    
     @
  )   H
     r
  	   �
     �
      �
     �
     �
  
   �
     �
  g   �
  �  O  �     +   �          %     A     I     U     g     }     �  d   �  
      #     	   /  ^   9     �     �  "   �     �     �     �  ^         _  �   p     �  	   �       )        9     B  <   J     �     �  	   �     �     �  	   �     �      �               &  �   ,     �  I   �       4        S     l  	   s      }     �     �  
   �     �  \   �     '              
       !                  *          (   "      #           7                                              /   %   2       )   1      6                                  0              3      8       4   $   9         5   -             +       ,      .   &   	           Here you'll get to show us who is the awesome superstar behind this blog.<br> Get creative and write a intro about you, snap a awesome closeup and give your name and email. A blog theme with DustPress and CSS grid. Activation Key Required Appears on top of the hero image. Archive Author Author image Author introduction Author name Basic info Can't load archives right now. If site reload doesn't help please contant webmaster@leijonaa.fi Categories Choose up to three articles Comment Email address is obfuscated so it's nonsense for bots, but readable for humans. End First Helper missing parameter "s". Hero text Hi! Nice to meet you. Home Home logo by <a href="https://www.flaticon.com/authors/freepik">Freepik</a> I love this! If set, blog's hero image is located after the top menu in front page. <br/>
	Minimum image size: 1920 x 1280 pixels. <br/>
	Image ratio 4 : 1. Instructions Last Leijonaa Link to Loading Menu Minimum image size is 150 x 150 pixels. 
	Image aspect ratio is 1 : 1. Newest posts Next Previous Previous and next Primary Navigation Read also Relevant posts Show related articles Start Submit Tags The user gets these as recommendation after this article. <br>
	If less than three articles are chosen, the rest is fetched automatically from the same category. Theme Settings Theme by <a href="https://github.com/pikkulahti">Pikkulahti</a> Twitter Used as title of the author info section. Your account is now active! Your name comments http://www.github.com/pikkulahti likes page pikkulahti your@email.com ~40 characters per line. <br/>
	No maximum limit but please consider using no more than 400 characters. Project-Id-Version: Leijonaa
POT-Creation-Date: 2019-03-16 13:08+0200
PO-Revision-Date: 2019-03-16 13:09+0200
Last-Translator: 
Language-Team: 
Language: fi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.2.1
X-Poedit-Basepath: ..
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-Flags-xgettext: --add-comments=translators:
X-Poedit-WPHeader: style.css
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;_nx_noop:3c,1,2;__ngettext_noop:1,2
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: *.js
  Täällä pääset esittelemään lukijoillesi, kuka onkaan tämä mahtava tyyppi blogin takana. <br> Heittäydy luovaksi ja kirjoita itsestäsi lyhyt intro, nappaa mahtava kuva ja täytä nimi ja sähköposti. Blogiteema DustPressillä ja CSS-gridillä. Aktivointiavain vaaditaan Näkyy herokuvan päällä. Arkisto Kirjoittaja Kirjoittajan kuva Kirjoittajan esittely Kirjoittajan nimi Perustiedot Ei voi ladata arkistoja juuri nyt. Jos sivuston uudelleenlataus laita viestiä webmaster@leijonaa.fi Kategoriat Valitse enintään kolme artikkelia Kommentti Sähköpostiosoite obfuskoidaan, jotta se on boteille siansaksaa, mutta ihmisille luettavissa. Loppu Ensimmäinen Helperistä puuttuu parametri "s". Herokuvan teksti Moikka! Hauska tutustua. Etusivu Etusivu-linkin logo tekijä: <a href="https://www.flaticon.com/authors/freepik" > Freepik </a> Rakastan tätä! Jos asetettu, blogin herokuva näkyy ennen valikkoa etusivulla. <br/> Pienin kuva koko: 1920 x 1280 pikseliä. <br/>
	Kuvasuhde 4:1. Ohjeet Viimeinen Leijonaa Linkki kirjoittajan profiiliin sivustolle Ladataan Valikko Minimikuvan koko on 150 x 150 pikseliä. 
	Kuvasuhde on 1:1. Uusimmat artikkelit Seuraava Edellinen Edellinen ja seuraava Päävalikko Lue myös Samantyylisiä artikkeleita Näytä samantyyliset artikkelit Alku Lähetä Tagit Käyttäjälle suositellaan näitä artikkeleita. <br> Jos valitset alle kolme artikkelia, loput haetaan automaattisesti samasta kategoriasta. Teeman asetukset Teeman tekijä: <a href="https://github.com/pikkulahti" > Pikkulahti </a> Twitter Käytetään Tietoa kirjoittajasta -osion otsikkona. Tunnuksesi on aktivoitu! Nimesi kommentit http://www.github.com/pikkulahti tykkäystä sivu pikkulahti sinun@sähköpostisi.com ~ 40 merkkiä per rivi. <br/> Ei enimmäisrajaa, mutta ota huomioon enintään 400 merkkiä. 