<?php

namespace leijonaa\CustomFields;

/**
 * Register theme options page.
 */
if ( \function_exists( 'acf_add_options_page' ) ) {

    \acf_add_options_page(
        array(
			'page_title' => __( 'Theme Settings', 'leijonaa' ),
			'menu_title' => __( 'Theme Settings', 'leijonaa' ),
			'menu_slug'  => 'theme-settings',
			'capability' => 'edit_posts',
			'redirect'   => false,
			'icon_url'   => 'dashicons-admin-customizer',
			'position'   => 15,
        )
    );
}


/**
 * Register author introduction fields to theme settings.
 */
if ( \function_exists( 'acf_add_local_field_group' ) ) {
    acf_add_local_field_group(
        array(
			'key'        => 'ljn_author-fields',
			'title'      => __( 'Hi! Nice to meet you.', 'leijonaa' ),
			'fields'     => array(
				array(
					'key'       => 'ljn_author-message',
					'type'      => 'message',
					'message'   => __( ' Here you\'ll get to show us who is the awesome superstar behind this blog.<br> Get creative and write a intro about you, snap a awesome closeup and give your name and email.', 'leijonaa' ),
					'new_lines' => 'wpautop',
					'esc_html'  => 0,
				),
				array(
					'key'        => 'ljn_author-wrapper',
					'label'      => __( 'Basic info', 'leijonaa' ),
					'name'       => 'ljn_author-wrapper',
					'type'       => 'repeater',
					'wrapper'    => array(
						'width' => '50',
					),
					'min'        => 1,
					'max'        => 1,
					'layout'     => 'block',
					'sub_fields' => array(
						array(
							'key'          => 'ljn_author-name',
							'label'        => __( 'Author name', 'leijonaa' ),
							'name'         => 'ljn_author-name',
							'type'         => 'text',
							'instructions' => __( 'Used as title of the author info section.', 'leijonaa' ),
							'placeholder'  => __( 'Your name',  'leijonaa' ),
						),
						array(
							'key'          => 'ljn_author-email',
							'label'        => 'Author email',
							'name'         => 'ljn_author-email',
							'type'         => 'email',
							'instructions' => __( 'Email address is obfuscated so it\'s nonsense for bots, but readable for humans.', 'leijonaa' ),
							'placeholder'  => __( 'your@email.com', 'leijonaa' ),
						),
						array(
							'key'           => 'ljn_author-image',
							'label'         => __( 'Author image', 'leijonaa' ),
							'name'          => 'ljn_author-image',
							'type'          => 'image',
							'instructions'  => __(
								'Minimum image size is 150 x 150 pixels. 
	Image aspect ratio is 1 : 1.', 'leijonaa'
                            ),
							'return_format' => 'array',
							'preview_size'  => 'thumbnail',
							'library'       => 'all',
							'min_width'     => 150,
							'min_height'    => 150,
						),
					),
				),
				array(
					'key'          => 'ljn_author-intro',
					'label'        => __( 'Author introduction', 'leijonaa' ),
					'name'         => 'ljn_author-intro',
					'type'         => 'wysiwyg',
					'instructions' => __(
						'~40 characters per line. <br/>
	No maximum limit but please consider using no more than 400 characters.', 'leijonaa'
                    ),
					'wrapper'      => array(
						'width' => '50',
					),
					'tabs'         => 'all',
					'toolbar'      => 'basic',
					'media_upload' => 0,
				),
			),
			'location'   => array(
				array(
					array(
						'param'    => 'options_page',
						'operator' => '==',
						'value'    => 'theme-settings',
					),
				),
			),
			'menu_order' => 0,
        )
    );
    /**
     * Register hero image field to theme settings page.
     */
    acf_add_local_field_group(
        array(
			'key'                   => 'ljn_hero-fields',
			'title'                 => 'Hero image',
			'fields'                => array(
				array(
					'key'           => 'ljn_hero-image',
					'label'         => __( 'Instructions', 'leijonaa' ),
					'name'          => 'ljn_hero-image',
					'type'          => 'image',
					'instructions'  => __(
						'If set, blog\'s hero image is located after the top menu in front page. <br/>
	Minimum image size: 1920 x 1280 pixels. <br/>
	Image ratio 4 : 1.', 'leijonaa'
                    ),
					'wrapper'       => array(
						'class' => 'ljn_hero-image',
					),
					'return_format' => 'array',
					'preview_size'  => 'medium_large',
					'min_width'     => 1920,
					'min_height'    => 1280,
				),
                array(
                'key' => 'ljn_hero-top-layer-type',
                'label' => 'Hero top layer type',
                'name' => 'ljn_hero-top-layer-type',
                'type' => 'select',
                'instructions' => 'Choose a type',
                'required' => 1,
                'conditional_logic' => 0,
                'choices' => array (
                    'blog_name' => 'Blog name',
                    'custom_text' => 'Custom text',
                    'no_layer' => 'No layer',
                ),
                'default_value' => array (
                    'blog_name'
                ),
                'allow_null' => 0,
                'multiple' => 0,
                'return_format' => 'value',
                ),
				array(
					'key'          => 'ljn_hero-top-layer-text',
					'label'        => __( 'Hero text', 'leijonaa' ),
					'name'         => 'ljn_hero-text',
					'type'         => 'text',
					'instructions' => __( 'Appears on top of the hero image.', 'leijonaa' ),
					'placeholder'  => __( 'Hero text',  'leijonaa' ),
					'required'     => 1,
                    'conditional_logic' => array(
                        array(
                            array(
                                'field'    => 'ljn_hero-top-layer-type',
                                'operator' => '==',
                                'value'    => 'custom_text',
                            ),
                        ),
                    ),
                ),
			),
			'location'              => array(
				array(
					array(
						'param'    => 'options_page',
						'operator' => '==',
						'value'    => 'theme-settings',
					),
				),
			),
			'menu_order'            => 1,
			'label_placement'       => 'left',
			'instruction_placement' => 'label',
			'active'                => 1,
        )
    );

    acf_add_local_field_group(
        array(
			'key'                   => 'ljn_social-media',
			'title'                 => 'Social media',
			'fields'                => array(
				array(
					'key'               => 'ljn_social-media-accounts',
					'label'             => 'Social media accounts',
					'name'              => 'ljn_social-media-accounts',
					'type'              => 'repeater',
					'instructions'      => '',
					'required'          => 0,
					'conditional_logic' => 0,
					'wrapper'           => array(
						'width' => '',
						'class' => '',
						'id'    => '',
					),
					'collapsed'         => '',
					'min'               => 0,
					'max'               => 0,
					'layout'            => 'table',
					'button_label'      => '',
					'sub_fields'        => array(
						array(
							'key'               => 'ljn_social-media-type',
							'label'             => 'Platform',
							'name'              => 'ljn_social-media-type',
							'type'              => 'select',
							'instructions'      => 'Choose a service',
							'required'          => 1,
							'conditional_logic' => 0,
							'wrapper'           => array(
								'width' => '',
								'class' => '',
								'id'    => '',
							),
							'choices'           => array(
								'facebook'  => 'Facebook',
								'instagram' => 'Instagram',
								'linkedin'  => 'LinkedIn',
								'pinterest' => 'Pinterest',
								'twitter'   => 'Twitter',
								'youtube'   => 'Youtube',
							),
							'default_value'     => array(),
							'allow_null'        => 0,
							'multiple'          => 0,
							'ui'                => 0,
							'ajax'              => 0,
							'return_format'     => 'value',
							'placeholder'       => '',
						),
						array(
							'key'               => 'ljn_social-media-link',
							'label'             => 'Link',
							'name'              => 'ljn_social-media-link',
							'type'              => 'url',
							'instructions'      => '',
							'required'          => 1,
							'conditional_logic' => 0,
							'wrapper'           => array(
								'width' => '',
								'class' => '',
								'id'    => '',
							),
							'default_value'     => '',
							'placeholder'       => '',
						),
					),
				),
			),
			'location'              => array(
				array(
					array(
						'param'    => 'options_page',
						'operator' => '==',
						'value'    => 'theme-settings',
					),
				),
			),
			'menu_order'            => 2,
			'position'              => 'normal',
			'style'                 => 'default',
			'label_placement'       => 'top',
			'instruction_placement' => 'label',
			'hide_on_screen'        => '',
			'active'                => 1,
			'description'           => '',
        )
    );

    acf_add_local_field_group(
        array(
            'key'                   => 'ljn_twitter',
            'title'                 => 'Twitter feed credentials',
            'fields'                => array(
                array(
                    'key'               => 'ljn_twitter-consumer_key',
                    'label'             => 'Consumer key',
                    'name'              => 'ljn_twitter-consumer_key',
                    'type'              => 'text',
                    'instructions'      => '',
                    'required'          => 1,
                    'conditional_logic' => 0,
                    'default_value'     => '',
                    'placeholder'       => '',
                ),
                array(
                    'key'               => 'ljn_twitter-consumer_secret',
                    'label'             => 'Consumer secret     ',
                    'name'              => 'ljn_twitter-consumer_secret',
                    'type'              => 'text',
                    'instructions'      => '',
                    'required'          => 1,
                    'conditional_logic' => 0,
                    'default_value'     => '',
                    'placeholder'       => '',
                ),
                array(
                    'key'               => 'ljn_twitter-access_token',
                    'label'             => 'Access token',
                    'name'              => 'ljn_twitter-access_token',
                    'type'              => 'text',
                    'instructions'      => '',
                    'required'          => 1,
                    'conditional_logic' => 0,
                    'default_value'     => '',
                    'placeholder'       => '',
                ),
                array(
                    'key'               => 'ljn_twitter-access_token_secret',
                    'label'             => 'Access Token Secret',
                    'name'              => 'ljn_twitter-access_token_secret',
                    'type'              => 'text',
                    'instructions'      => '',
                    'required'          => 1,
                    'conditional_logic' => 0,
                    'default_value'     => '',
                    'placeholder'       => '',
                ),
            ),
            'location'              => array(
                array(
                    array(
                        'param'    => 'options_page',
                        'operator' => '==',
                        'value'    => 'theme-settings',
                    ),
                ),
            ),
            'menu_order'            => 3,
            'position'              => 'normal',
            'style'                 => 'default',
            'label_placement'       => 'top',
            'instruction_placement' => 'label',
            'hide_on_screen'        => '',
            'active'                => 1,
            'description'           => '',
        )
    );

    acf_add_local_field_group(
        array(
            'key'                   => 'ljn_instagram',
            'title'                 => 'Instagram feed credentials',
            'fields'                => array(
                array(
                    'key'               => 'ljn_instagram-access-token',
                    'label'             => 'Consumer key',
                    'name'              => 'ljn_instagram-access-token',
                    'type'              => 'text',
                    'instructions'      => '',
                    'required'          => 1,
                    'conditional_logic' => 0,
                    'default_value'     => '',
                    'placeholder'       => '',
                ),
            ),
            'location'              => array(
                array(
                    array(
                        'param'    => 'options_page',
                        'operator' => '==',
                        'value'    => 'theme-settings',
                    ),
                ),
            ),
            'menu_order'            => 4,
            'position'              => 'normal',
            'style'                 => 'default',
            'label_placement'       => 'top',
            'instruction_placement' => 'label',
            'hide_on_screen'        => '',
            'active'                => 1,
            'description'           => '',
        )
    );
}
