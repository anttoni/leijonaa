<?php
/**
 * Translations for JavaScript.
 */
namespace leijonaa;

/**
 * Class JsLocalization
 *
 * @package leijonaa
 */
class JsLocalization {

    /**
     * Initializes JavaScript localization.
     */
    public static function init() {
        add_action( 'wp_footer', array( __NAMESPACE__ . '\\JsLocalization', 'print_localization' ), 10, 0 );
    }

    /**
     * Contains translated string for JavaScript.
     *
     * @return array
     */
    protected static function translations() {
        $translations = array(
            'archive_error_message' => __( 'Can\'t load archives right now. If site reload doesn\'t help please contant webmaster@leijonaa.fi', 'leijonaa' ),
        );

        $translations = apply_filters( 'ljn_js_translations', $translations );

        return $translations;
    }

    /**
     * Prints out all translations for JavaScript int 'wp_footer' hook.
     */
    public static function print_localization() {
        $translations = self::translations();

        echo '<script> ' . PHP_EOL;
        echo 'var ljn_js_translations = ' . \wp_json_encode( $translations ) . PHP_EOL;
        echo '</script> ' . PHP_EOL;
    }
}

JsLocalization::init();
