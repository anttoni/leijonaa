<?php
namespace Leijonaa;

/**
 * The instagram feed class, uses instagram's api to retrieve posts
 */
class Instagram {

    /**
     * Get posts from instagram.
     *
     * @param int $count Number of posts
     *
     * @return array
     */
    public static function instagram_posts( $count = 10 ) {

        $token = \get_field( 'ljn_instagram-access-token', 'option' );
        if( $token ) {
            $data = [];
            $url  = 'https://api.instagram.com/v1/users/self/media/recent/';
            $url  = \add_query_arg( 'access_token', $token, $url );
            $url  = \add_query_arg( 'count', $count, $url );

            $response      = \wp_remote_get( $url );
            $api_data      = json_decode( $response['body'] );
            $feed          = array();
            $feed['posts'] = array();

            foreach ( $api_data->data as $post ) {

                $formatted_post = array(
                    'img'     => $post->images->standard_resolution,
                    'time'    => date( 'd.m.Y, H:i', $post->created_time ),
                    'caption' => $post->caption->text,
                    'type'    => $post->type,
                    'tags'    => $post->tags,
                    'url'     => $post->link,
                );

                if ( $formatted_post['type'] === 'carousel' ) {
                    foreach ( $post->carousel_media as $media ) {
                        $formatted_post['carousel_images'][] = $media->images->standard_resolution;
                    }
                }

                if ( $formatted_post['type'] === 'video' ) {
                    $formatted_post['video'] = $post->videos->standard_resolution;
                }

                $feed['posts'][] = $formatted_post;
                unset( $formatted_post );
            }
            unset( $post );

            $feed['user'] = $api_data->data[0]->user;

            return $feed;
        }
    }
}
