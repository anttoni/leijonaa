<?php
/**
 * Site commenting functionalities.
 */

namespace leijonaa;

/**
 * Class Comments
 *
 * @package leijonaa
 */
class Comments {

    /**
     * Add Dustpress comment hooks.
     */
    public static function init() {
        \add_filter(
            'dustpress/comments/ljn/get_form_args',
            array( __NAMESPACE__ . '\\Comments', 'get_form_args' ), 1, 2
        );
        \add_filter(
            'dustpress/comments/ljn/get_comments_args',
            array( __NAMESPACE__ . '\\Comments', 'get_comments_args' ), 1, 2
        );
    }

    /**
     * Define comment arguments.
     *
     * @param array $args    Comment arguments.
     * @param int   $post_id Current post id (the post that is being commented).
     *
     * @return array Edited comment arguments.
     */
    public static function get_comments_args( $args, $post_id ) {
        $args['reply'] = true;
        return $args;
    }

    /**
     * Define comment form arguments.
     *
     * @param array $args    Comment form arguments.
     * @param int   $post_id Current post id.
     *
     * @return array Edited comment form arguments.
     */
    public static function get_form_args( $args, $post_id ) {
        $args['title_reply']  = __( 'Comment', 'POF' );
        $args['label_submit'] = __( 'Submit', 'POF' );
        $args['class_submit'] = 'button radius';
        $args['remove_input'] = array( 'url' );
        $args['input_class']  = 'radius';
        return $args;
    }

}

Comments::init();