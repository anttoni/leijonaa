<?php

namespace leijonaa\CustomFields;

/**
 * Register menu options.
 */
if( \function_exists('acf_add_options_page') ) {

    acf_add_local_field_group(array (
        'key' => 'ljn_menu-settings',
        'title' => 'Menu settings',
        'fields' => array (
            array (
                'key' => 'ljn_menu-settings-social-media',
                'label' => 'Social media links',
                'name' => 'ljn_menu-settings-social-media',
                'type' => 'true_false',
                'instructions' => 'Show social media links in menu?',
                'required' => 0,
                'message' => '',
                'default_value' => 1,
            ),
        ),
        'location' => array (
            array (
                array (
                    'param' => 'nav_menu',
                    'operator' => '==',
                    'value' => 'location/primary_navigation',
                ),
            ),
        ),
        'menu_order' => 0,
        'position' => 'normal',
        'style' => 'default',
        'label_placement' => 'top',
        'instruction_placement' => 'label',
        'hide_on_screen' => '',
        'active' => 1,
        'description' => '',
    ));


}