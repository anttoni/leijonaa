<?php
/**
 * Extra theme functionalities.
 */

namespace leijonaa\Extras;

use leijonaa\Setup;


function add_post_meta( $post ) {

      //@TODO Cache

      // Post tags
      $post->tags = array();
      // Returns either an array of objects or an empty array
      $tags = wp_get_post_tags( $post->ID );

      $post->url = get_permalink( $post->ID );

      // Loop through each tag and get tag name and link.
      foreach ( $tags as $tag ) {
          $post->tags[] = array(
              'tag_name' => $tag->name,
              'tag_link' => get_tag_link( $tag->term_id ),
          );
      }

      // Post categories
      $post->categories = array();
      $categories       = get_the_category( $post->ID );
      // Used when fetchin relevant posts
      $categories_for_relevant = [];

      // Loop through each category and get category name and link.
      foreach ( $categories as $category ) {
          $post->categories[] = array(
              'category_name' => $category->name,
              'category_link' => get_category_link( $category->term_id ),
          );
          $categories_for_relevant[] = $category->term_id;
      }

      // Get relevant posts
      $relevant = get_field( 'relevant', $post->ID );
      $idx = 0;
      $post->relevant = array();
      if ( $relevant ) {

          foreach ( $relevant as $relevant_post ) {
              $post->relevant[] = array(
                  'title'   => $relevant_post->post_title,
                  'url'     => get_permalink( $relevant_post->ID ),
                  'content' => $relevant_post->post_content,
              );

              if( ++$idx >= 3 ) {
                  break;
              }
          }
          unset( $relevant_post );

      }

      if ( $idx < 2 && ! empty( $categories_for_relevant ) ) {

          $args = [
              'post_type'              => 'post',
              'post_visibility'        => 'published',
              'posts_per_page'         =>  10,
              'orderby'                => 'date',
              'order'                  => 'DESC',
              'category__in'           => $categories_for_relevant,
              'update_post_meta_cache' => false,
              'update_post_term_cache' => false,
              'no_found_rows'          => true,
              'query_object'           => false,
          ];

          $relevant_from_categories = get_posts( $args );

          shuffle( $relevant_from_categories );

          foreach ( $relevant_from_categories as $relevant_post ) {
              $post->relevant[] = array(
                  'title'   => $relevant_post->post_title,
                  'url'     => get_permalink( $relevant_post->ID ),
                  'content' => $relevant_post->post_content,
              );

              if ( ++$idx >= 3 ) {
                  break;
              }
          }
      }

      return $post;
}

/**
 * Gets next and previous post
 *
 * @param [type] $id
 * @return void
 */
function get_adjacent_posts( $id ) {

    global $post;
    $backup = false;

    if ( $post->ID !== $id  ) {
        $backup = $post;
        $post = get_post( $id );
    }

    $posts = [];

    $previous = get_previous_post();
        if ( $previous ) {
        $previous->url = get_permalink( $previous->ID );
        $posts['previous'] = $previous;
    }

    $next = get_next_post();
        if ( $next ) {
        $next->url = get_permalink( $next->ID );
        $posts['next'] = $next;
    }

    if ( $backup ) {
      $post = $backup;
    }

    return $posts;

}

/**
 * Contains theme credits
 * @return array
 */
function roll_the_credits() {

  $credits = [
    __( 'Theme by <a href="https://github.com/pikkulahti">Pikkulahti</a>', 'leijonaa' ),
    __( 'Home logo by <a href="https://www.flaticon.com/authors/freepik">Freepik</a>', 'leijonaa' ),
  ];

  return $credits;
}
