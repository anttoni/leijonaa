<?php
/**
 * Sidebar data functions.
 */

namespace leijonaa;

/**
 * Class Sidebar
 *
 * @package leijonaa
 */
class Sidebar {

    /**
     * Fetches author data from options and stores it under MiddleModel class.
     */
    public static function get_author() {



        $author = array();

        // Check that acf is initialized.
        if ( function_exists( 'get_field' ) ) {

            // Get author name, image and email as array.
            $author_data = get_field( 'ljn_author-wrapper','option' );

            // Get author email from the array containing array.
            // The parent array can only have one item so we can use [0].
            $raw_email = $author_data[0]['ljn_author-email'];

            // If email is set, obfuscate it.
            $email = ! empty( $raw_email ) ? antispambot( $raw_email ) : '';

            // Restructure author data for view.
            $author = array(
                'intro' => get_field( 'ljn_author-intro', 'option' ),
                'image' => $author_data[0]['ljn_author-image'],
                'name'  => $author_data[0]['ljn_author-name'],
                'email' => $email,
                'social' => get_field( 'ljn_social-media-accounts', 'option' ),
            );
        }

        return $author;
    }

    /**
     * Fetches five newest posts and stores them under MiddleModel class.
     */
    public static function get_newest() {

        $args = [
            'post_type'              => 'post',
            'posts_per_page'         => 5,
            'update_post_meta_cache' => false,
            'update_post_term_cache' => false,
            'no_found_rows'          => true,
            'query_object'           => false,
        ];

        $query = new \WP_Query( $args );

        $newest = array();

        // If we have posts
        if ( isset( $query->posts ) ) {
            // Loop through posts and take needed data.
            foreach ( $query->posts as $post ) {
                $article            = new \stdClass();
                $article->permalink = get_permalink( $post->ID );
                $article->title     = $post->post_title;
                $newest[]           = $article;
            }
        }

        \wp_reset_postdata();

        return $newest;
    }

    /**
     * Get five tweets.
     *
     * @return array
     */
    public static function get_twitter() {
        $twitter = new Twitter();
        $tweets  = $twitter->get_status( 2 );
        return $tweets;
    }
}
