<?php
/**
 * Handles likes
 */

namespace leijonaa;

/**
 * Class Likes
 *
 * @package leijonaa
 */
class Likes {

    /**
     * Post meta key for post likes.
     *
     * @var string
     */
    private static $meta_key = 'ljn_post_likes';

    /**
     * Add hooks.
     *  wp_ajax_ $function _likes
     */
    public static function init() {

        // This class' functions to register for ajax.
        $functions = array(
            'add',
            'remove',
            'get',
        );

        foreach ( $functions as $function ) {
            add_action( 'wp_ajax_' . $function . '_likes', [ __CLASS__, $function ], 10, 1 );
            add_action( 'wp_ajax_nopriv_' . $function . '_likes', [ __CLASS__, $function ], 10, 1 );
        }
    }

    /**
     * Add one like to specific post.
     *
     * @param int $id Post id.
     * @return int $likes
     */
    public static function add() {

        $id = $_POST['id'];

        $likes = self::get( $id );

        $likes = $likes + 1;

        update_post_meta( $id, self::$meta_key, $likes );

        return( 1 );
    }

    /**
     * Remove one like from specific post.
     *
     * @param int $id Post id.
     * @return int $likes
     */
    public static function remove() {

        $id = $_POST['id'];

        $likes = self::get( $id );

        $likes = $likes - 1;

        update_post_meta( $id, self::$meta_key, $likes );

        return( $likes );
    }

    /**
     * Get number of likes from specific post.
     *
     * @param int $id Post id.
     * @return int $likes
     */
    public static function get( $id ) {
        $likes = get_post_meta( $id, self::$meta_key, true );

        if ( empty( $likes ) ) {
            $likes = 0;
        }

        return $likes;
    }

}

Likes::init();
