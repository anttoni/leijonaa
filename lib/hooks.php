<?php
/**
 * Extra theme functionalities.
 */

namespace leijonaa\Hooks;

use leijonaa\Setup;

/**
 * Rewrite the loadmore
 */
function page_rewrite() {
    // Pagination for all pages.
    $page = __('page', 'leijonaa');
    add_rewrite_rule( '^(.+)/' . $page . '/([^/])?(/)?$', 'index.php?pagename=$matches[1]&paged=$matches[2]', 'top' );
}
add_action( 'init', __NAMESPACE__ . '\\page_rewrite', 999, 0 );


/**
 * Add <body> classes
 *
 * @param array $classes Body class strings.
 *
 * @return array
 */
function body_class( $classes ) {
    // Add page slug if it doesn't exist
    if ( \is_single() || \is_page() && ! \is_front_page() ) {
        if ( ! in_array( basename( get_permalink() ), $classes ) ) {
            $classes[] = basename( get_permalink() );
        }
    }

    return $classes;
}

\add_filter( 'body_class', __NAMESPACE__ . '\\body_class' );

/**
 * This changes the default pagination and search base.
 */
function translate_base_rewrite() {
    global $wp_rewrite;
    $wp_rewrite->pagination_base = __( 'page', 'leijonaa');
}
add_action( 'init', __NAMESPACE__ . '\\translate_base_rewrite', 999, 0 );
