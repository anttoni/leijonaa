<?php

namespace leijonaa;

use \Abraham\TwitterOAuth\TwitterOAuth as TwitterApi;

/**
 * Themes Twitter api
 */
class Twitter {

    /**
     * Initialize Twitter Api
     */
    public function __construct() {
        $this->consumer_key        = \get_field( 'ljn_twitter-consumer_key ', 'option' );
        $this->consumer_secret     = \get_field( 'ljn_twitter-consumer_secret ', 'option' );
        $this->access_token        = \get_field( 'ljn_twitter-access_token', 'option' );
        $this->access_token_secret = \get_field( 'ljn_twitter-access_token_secret', 'option' );

        $this->connection = new TwitterApi(
            $this->consumer_key,
            $this->consumer_secret,
            $this->access_token,
            $this->access_token_secret
        );
    }

    /**
     * Get twitter statuses
     *
     * @param integer $items How many items to get.
     * @return object $statuses
     */
    public function get_status( $items = 10 ) {
        $statuses = $this->connection->get(
            'statuses/user_timeline', [
				'count'           => $items,
				'exclude_replies' => true,
			]
        );

        return $statuses;
    }
}
