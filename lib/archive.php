<?php
/**
 * Sidebar archive
 */

namespace leijonaa;

/**
 * Class Archive
 *
 * @package leijonaa
 */
class Archive {

    /**
     * Class' cache key.
     *
     * @var string
     */
	public static $cache_key = 'ljn_archive_cache';

    /**
     * Add hooks for archive.
     */
	public static function init() {
	    //\add_action( 'save_post', array( __NAMESPACE__ . '\\Archive', 'update_cache' ), 10, 3 );
	}

    /**
     * Updates saved post in cache.
     */
    public static function update_cache( $id, $post, $update ) {
        if ( ! \wp_doing_ajax() && ! \wp_is_post_autosave( $id ) ) {
            if ( $post->post_type === 'post' && $post->status !== 'publish' && ! $update ) {
                self::remove_post( $post );
            } elseif ( $post->post_type === 'post' && $update ) {
                self::update_post( $post );
            }
        }
    }

    /**
     * Get cache
     *
     * @return array
     */
    public static function get() {
        $cache = \get_transient( self::$cache_key ) ?: array();
        return $cache;
    }

    /**
     * Add post to cache.
     *
     * @param array $posts
     */
    public static function set( $posts, $keep_posts = true ) {

        if ( $keep_posts ) {
            $cache         = self::get();
            $posts = array_merge( $cache, $posts );
        }
        \set_transient( self::$cache_key, $posts, YEAR_IN_SECONDS );
    }

    /**
     * Updates post in cache.
     *
     * @param object $post Post object.
     */
    public static function update_post( $post ) {
        $cache = array();

        // Change values to strings so array_merge in set() doesn't append them.
        $post_date = strtotime( $post->post_date );
        $month_int = date( 'n', $post_date );
        $month_str = date( 'F', $post_date );
        $year      = date( 'Y', $post_date );
        $url       = \get_permalink( $post->ID );

        $formatted_posts[ $year ]['year']                                       = $year;
        $formatted_posts[ $year ]['months'][ $month_int ]['month']              = $month_str;
        $formatted_posts[ $year ]['months'][ $month_int ]['posts'][ $post->ID ] = array(
            'title' => $post->post_title,
            'url'   => $url,
        );

        self::set( $cache, true );
    }

    /**
     * Remove post from cache.
     *
     * @param $id
     */
    public static function remove_post( $post ) {
        $cache = self::get();

        $post_date = strtotime( $post->post_date );
        $month     = date( 'n', $post_date );
        $year      = date( 'Y', $post_date );

        unset( $cache[ $year ]['months'][ $month ]['posts'][ $post->ID ] );

        \set_transient( self::$cache_key, $cache, YEAR_IN_SECONDS );
    }

    /**
     * Generate the cache. Executed only with wp-cli.
     */
    public static function generate() {

        // All published posts
        $args = array(
            'post_status'    => 'publish',
            'post_type'      => 'post',
            'posts_per_page' => -1,
        );

        $query = new \WP_Query( $args );

        if ( $query->found_posts > 0 ) {
            $posts           = $query->posts;
            $formatted_posts = array();

            foreach ( $posts as $post ) {
                $post_date = strtotime( $post->post_date );
                $month_int = date( 'n', $post_date );
                $month_str = date( 'F', $post_date );
                $year      = date( 'Y', $post_date );
                $url       = \get_permalink( $post->ID );

                $formatted_posts[ $year ]['year']                                   = $year;
                $formatted_posts[ $year ]['months'][ $month_int ]['month']              = $month_str;
                $formatted_posts[ $year ]['months'][ $month_int ]['posts'][ $post->ID ] = array(
                    'title' => $post->post_title,
                    'url'   => $url,
                );
            }

            self::set( $formatted_posts, false );
        }
    }

}

Archive::init();
