<?php
namespace leijonaa;

class Utility {

    /**
     * Return category object with links.
     */
    public static function categories_with_links() {
        $categories = get_categories();

        if ( ! empty( $categories ) ) {
            $categories = array_map( function( $category ) {
                $category->link = get_category_link( $category->term_id );
                return $category;
            }, $categories );
        }

        return $categories;
    }
}