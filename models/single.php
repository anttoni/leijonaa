<?php

/**
 * This is the model for singular posts.
 */
class Single extends MiddleModel {

    /**
     * This returns the current post
     *
     * @return array|null|WP_Post
     */
    public function post() {

        $cache_key = 'ljn-' . __CLASS__ . '-' . __FUNCTION__ . '-' . get_the_ID();

        $post = \wp_cache_get( $cache_key );

        if ( ! empty( $post ) ) {
            // return $posts;
        }

        $post = \DustPress\Query::get_acf_post( get_the_ID() );
        $post = \leijonaa\Extras\add_post_meta( $post );
        $post->adjacent = \leijonaa\Extras\get_adjacent_posts( $post->ID );

        \wp_cache_set( $cache_key, $post, self::$post_group_key, YEAR_IN_SECONDS );

        return $post;
    }
}
