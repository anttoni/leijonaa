<?php

/**
 * This is the default model class for our theme.
 */
class Index extends MiddleModel {
    /**
     * Get posts and handle from wp_query or cache if set
     *
     * @return array
     */
    public function query() {

        global $wp_query;

        $paged     = ( \get_query_var( 'paged' ) ) ? \get_query_var( 'paged' ) : 1;

        $cache_key = 'ljn-' . __CLASS__ . '-' . __FUNCTION__ . '/' . $paged ;

        $data = \wp_cache_get( $cache_key );

        if ( ! empty( $data ) ) {
            // return $data;
        }

        $posts     = $wp_query->posts;
        $page      = $paged;
        $per_page  = $wp_query->query_vars['posts_per_page'];
        $items     = (int) $wp_query->found_posts;

        // If posts, modify and return them.
        if ( ! empty( $posts ) ) {

            // Add categories and tags under posts data.
            foreach ( $posts as &$post ) {
                $post = \leijonaa\Extras\add_post_meta( $post );
            }
            unset( $post );

            $data                       = new \stdClass();
            $data->posts                = $posts;
            $data->pagination           = new \stdClass();
            $data->pagination->page     = $page;
            $data->pagination->per_page = $per_page;
            $data->pagination->items    = $items;

            \wp_cache_set( $cache_key, $data, $cache_key, DAY_IN_SECONDS * 5 );

            return $data;
        }
    }
}
