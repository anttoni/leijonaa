<?php
/**
 * A middle model is used to wrap redundant data binding.
 */

/**
 * Class MiddleModel
 */
class MiddleModel extends \DustPress\Model {

    /*
     * Group key for all the caches that should be invalidated on post save.
     */
    public static $post_group_key = 'ljn_post_cache';

    /**
     * Functions§ accessible for DustPress.js
     *
     * @var array
     */
    protected $api = [
        'get_archive',
    ];

    /**
     * Constructor function
     *
     * @param array  $args Arguments.
     * @param object $parent Parent.
     */
    public function __construct( $args = [], $parent = null ) {
        $document_class = get_class( $this );
        add_filter( 'dustpress/data/wp', function( $data ) use ( $document_class ) {
            $data['document_class'] = apply_filters( 'dustpress/document_class', [ $document_class ] );
            return $data;
        });
        parent::__construct( $args, $parent );
    }


    /**
     * Binds submodules for all extending classes.
     */
    public function submodules() {
        $this->bind_sub( 'Header' );
        $this->bind_sub( 'Footer' );
    }

    /**
     * Map and return sidebar data.
     */
    public function sidebar() {

        $sidebar = array(
            'Author'  => leijonaa\Sidebar::get_author(),
            'Newest'  => leijonaa\Sidebar::get_newest(),
            'Twitter' => leijonaa\Sidebar::get_twitter(),
        );

        return $sidebar;
    }

    /**
     * Fetches archives from cache.
     */
    protected function get_archive() {
        $archive = leijonaa\Archive::get();

        return $archive;
    }

    /**
     *  Return strings for localization.
     *
     * @return array
     */
    public static function l10n() {
		$strings = array(
			'ilove'        => __( 'I love this!', 'leijonaa' ),
			'likes'        => __( 'likes', 'leijonaa' ),
			'comments'     => __( 'comments', 'leijonaa' ),
            'archive'      => __( 'Archive', 'leijonaa' ),
            'author'       => __( 'Author', 'leijonaa' ),
            'twitter'      => __( 'Twitter', 'leijonaa' ),
            'newest_posts' => __( 'Newest posts', 'leijonaa' ),
            'read_also'    => __( 'Read also', 'leijonaa' ),
            'tags'         => __( 'Tags', 'leijonaa' ),
            'menu'         => __( 'Menu', 'leijonaa' ),
            'show-related' => __( 'Show related articles', 'leijonaa' ),
            'categories'   => __( 'Categories', 'leijonaa' ),
            'loading'      => __( 'Loading', 'leijonaa' ),
            'adjacent'     => __( 'Previous and next', 'leijonaa' ),
            'home'         => __( 'Home', 'leijonaa' ),
            'next'         => __( 'Next', 'leijonaa' ),
            'previous'     => __( 'Previous', 'leijonaa' ),
            'first'        => __( 'First', 'leijonaa' ),
            'last'         => __( 'Last', 'leijonaa' ),
            'start'        => __( 'Start', 'leijonaa' ),
            'end'          => __( 'End', 'leijonaa' ),
            'link_to'      => __( 'Link to'),
		);
		return $strings;
	}

}
