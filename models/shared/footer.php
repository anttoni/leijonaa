<?php
/**
 * Footer specific functions.
 */

/**
 * Class Footer
 */
class Footer extends \DustPress\Model {

    /**
     * Functions open for Dustpress.js
     *
     * @var array
     */
    protected $api = [
        'instagram',
    ];

    /**
     * Get posts from Instagram
     *
     * @return array
     */
    protected function instagram() {
        $posts = get_transient( 'leijonaa_instagram_posts' );

        if ( ! $posts ) {
            $posts = Leijonaa\Instagram::instagram_posts( 6 );
            set_transient( 'leijonaa_instagram_posts', $posts, DAY_IN_SECONDS );
        }
        return $posts;
    }
}
