<?php
/**
 * Header model.
 */

/**
 * Class Header
 */
class Header extends \DustPress\Model {

	/**
	 * Retrieve hero image for front page.
	 */
	public function hero_image() {
		if ( is_front_page() ) {
			$image = get_field( 'ljn_hero-image', 'option' );
			if ( ! empty( $image ) ) {
				return $image;
			}
		}
	}

    /**
     * Get hero text.
     *
     * @return string
     */
    public function hero_text() {
        if ( is_front_page() ) {
            $hero_type = get_field( 'ljn_hero-top-layer-type', 'option ' );

            $hero_text = '';

            if ( $hero_type[0] === 'blog_name' ) {
                $hero_text = get_bloginfo( 'name' );
            }
            elseif ( $hero_type[0] === 'custom_text' ) {
                $hero_text = get_field( 'ljn_hero-top-layer-text', 'option ' );
            }
            return $hero_text;
        }
    }

    /**
     * Translations from MiddlemModel.
     *
     * @return array
     */
    public function l10n() {
        $translations = MiddleModel::l10n();
        return $translations;
    }

    /**
     * Get data used in menu helper.
     *
     * @return array
     */
    public function menu_data() {

        $data_to_header = array(
            'translations'       => $this->l10n(),
            'template_directory' => get_template_directory_uri(),
            'home_url'           => site_url( '/', 'https'),
        );

        return $data_to_header;
    }


}