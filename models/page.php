<?php

/**
 * This is the model for single page.
 */
class Page extends MiddleModel {

    /**
     * This returns the current post
     *
     * @return array|null|WP_Post
     */
    public function post() {

        $cache_key = 'ljn-' . __CLASS__ . '-' . __FUNCTION__ . '-' . get_the_ID();

        $post = \wp_cache_get( $cache_key );

        if ( ! empty( $post ) ) {
            // return $posts;
        }

        $post = \DustPress\Query::get_acf_post( get_the_ID() );

        \wp_cache_set( $cache_key, $post, self::$post_group_key, YEAR_IN_SECONDS );

        return $post;
    }
}
